<header class="header">
  <div class="header__container">
    <div class="header__brand">
      <?php if ($logo):?>
        <div class="header__logo menuUp">
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          </a>
          <div class="navToggle">
            <a href="#">Menu</a>
          </div>
        </div>
      <?php endif; ?>
    </div>

    <?php if ($main_menu): ?>
      <nav>
        <div class="header__menu navigation">
          <?php print theme('links__system_main_menu', array(
            'links' => $main_menu,
            'attributes' => array(
              'id' => 'main-menu-links',
              'class' => array('links', 'clearfix'),
            ),
            'heading' => array(
              'text' => t('Main menu'),
              'level' => 'h2',
              'class' => array('element-invisible'),
            ),
          )); ?>
        </div>
      </nav>
    <?php endif; ?>

  </div>
</header>
