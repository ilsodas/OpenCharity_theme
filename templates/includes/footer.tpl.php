<?php
/**
 *
 * footer template
 *
 **/
?>

<footer class="footer">
  <div class="footer__container">
    <div class="footer__container--inner">
      <div class="footer__item footer__item--first col-12">
        <?php $boot_press_social = $opencharity_theme_social_media_links; ?>
        <ul class="footer__nav">
          <?php foreach($boot_press_social as $url => $icons): ?>
            <?php if($url): ?>
              <li>
                <a href="<?php print url(check_plain($url)); ?>" target="_blank">
                  <i class="<?php print $icons; ?>"></i>
                </a>
              </li>
            <?php endif; ?>
          <?php endforeach; ?>
        </ul>
      </div>

      <div class="footer__item footer__item--last col-12">
        <?php
        if (theme_get_setting('copyright_info')) {
          print filter_xss_admin(theme_get_setting('copyright_info'));
        }
        ?>
      </div>
    </div>
  </div>
</footer>
