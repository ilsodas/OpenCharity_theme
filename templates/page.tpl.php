<?php

/**
 * @file
 * Open Charity Theme - Drupal 7
 * Regions:
 * - $page['header']: Items for the header region.
 * - $page['featured']: Items for the featured region.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['content']: The main content of the current page.
 * - $page['content_top']: Items for the content top.
 * - $page['content_bottom']: Items for the content bottom.
 * - $page['banner']: Bannners section.
 * - $page['footer']: Items for the footer region.
 *
 */

?>
<div class="page-wrapper">
  <div class="page">

    <?php require_once _opencharity_theme_include_template('header.tpl.php'); ?>

    <?php if ($messages): ?>
      <div id="messages">
        <div class="section clearfix">
          <?php print $messages; ?>
        </div>
      </div>
    <?php endif; ?>

    <?php if ($page['banner']): ?>
      <div class="banner">
        <div class="banner__container">
          <?php print render($page['banner']); ?>
        </div>
      </div>
    <?php endif; ?>

    <div class="main-wrapper">
      <div class="main">

        <?php if ($breadcrumb): ?>
          <div class="breadcrumb">
            <?php print $breadcrumb; ?>
          </div>
        <?php endif; ?>

        <div class="content">
          <div class="content__container">
            <div class="content__container--inner">
              <?php if ($page['highlighted']): ?>
                <div class="highlighted">
                  <?php print render($page['highlighted']); ?>
                </div>
              <?php endif; ?>
              <a class="main-content"></a>
              <?php print render($title_prefix); ?>
              <?php if ($title): ?>
                <h1 class="main-content__title hidden">
                  <?php print $title; ?>
                </h1>
              <?php endif; ?>
              <?php if ($title): ?>
                <h2 class="main-content__title">
                  <?php print $title; ?>
                </h2>
              <?php endif; ?>
              <?php print render($title_suffix); ?>
              <?php if ($tabs): ?>
                <div class="main-content__tabs">
                  <?php print render($tabs); ?>
                </div>
              <?php endif; ?>
              <?php print render($page['help']); ?>
              <?php if ($action_links): ?>
                <ul class="main-content__action-links">
                  <?php print render($action_links); ?>
                </ul>
              <?php endif; ?>
              <?php print render($page['content']); ?>
              <?php print $feed_icons; ?>
            </div>
          </div>
        </div>

        <?php if ($page['content_bottom']): ?>
          <div class="content_bottom">
            <div class="content_bottom__container block">
              <div class="content_bottom__container--inner col-12">
                <?php print render($page['content_bottom']); ?>
              </div>
            </div>
          </div>
        <?php endif; ?>

      </div>
    </div>

  </div>
</div>

<?php require_once _opencharity_theme_include_template('footer.tpl.php'); ?>
