<?php
/**
 * Implements hook_form_FORM_ID_alter().
 *
 * @param $form
 * @param $form_state
 * The form state.
 *
 */

function opencharity_theme_form_system_theme_settings_alter(&$form, &$form_state) {

  $form['opencharity_theme_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Boot Press Theme Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['opencharity_theme_settings']['tabs'] = array(
    '#type' => 'vertical_tabs',
  );

  $form['opencharity_theme_settings']['tabs']['site_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Site Info'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['opencharity_theme_settings']['tabs']['site_info']['copyright_info'] = array(
    '#type' => 'textarea',
    '#title' => t('Copyright Info'),
    '#default_value' => theme_get_setting('copyright_info'),
    '#description'   => t('Enter your site copyright info'),
  );

  $form['opencharity_theme_settings']['tabs']['social_media'] = array(
    '#type' => 'fieldset',
    '#title' => t('Social Media'),
    '#description' => t('<strong>Enter your username / URL to show or leave blank to hide Social Media Icons</strong><hr>'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['opencharity_theme_settings']['tabs']['social_media']['facebook'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook URL'),
    '#size' => 10,
    '#default_value' => theme_get_setting('facebook'),
    '#description'   => t('Enter URL to your Facebook Account'),
  );

  $form['opencharity_theme_settings']['tabs']['social_media']['twitter'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter URL'),
    '#size' => 10,
    '#default_value' => theme_get_setting('twitter'),
    '#description'   => t('Enter URL to your Twitter Account'),
  );

  $form['opencharity_theme_settings']['tabs']['social_media']['googleplus'] = array(
    '#type' => 'textfield',
    '#title' => t('Google+ URL'),
    '#size' => 10,
    '#default_value' => theme_get_setting('googleplus'),
    '#description'   => t('Enter URL to your Google+ Account'),
  );

}
