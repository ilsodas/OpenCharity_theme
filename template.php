<?php

/**
 *
 * Preprocess variables for page template.
 *
 */
function opencharity_theme_preprocess_page(&$variables) {

  /* variable for the socialmedia links */
  $variables['opencharity_theme_social_media_links'] = array(
    theme_get_setting('facebook') => 'fa fa-facebook',
    theme_get_setting('twitter') => 'fa fa-twitter',
    theme_get_setting('googleplus') => 'fa fa-google-plus',
  );

}

/**
 * Include theme templates
 * @$tempate_file_name handel the template file name
 *
 */
function _opencharity_themes_include_template($tempate_file_name) {
  $theme_path = drupal_get_path('theme', 'boot_press');
  $theme_path .= '/templates/includes/' . $tempate_file_name;
  return $theme_path;
}

/**
 * Return absolute theme path.
 * @param $theme_name
 */
//function _boot_press_get_absolute_theme_path($theme_name) {
//  $absurl = url("", array('absolute' => true)) . drupal_get_path('theme', $theme_name);
//  return $absurl;
//}

/**
*
* Include theme templates
*
*/
function _opencharity_theme_include_template($tempate_file_name) {
  $theme_path = drupal_get_path('theme', 'opencharity_theme');
  $theme_path .= '/templates/includes/' . $tempate_file_name;
  return $theme_path;
}

function opencharity_theme_preprocess_html(&$variables) {
  drupal_add_css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css?family=fontawesome',array('type' => 'external'));
}
