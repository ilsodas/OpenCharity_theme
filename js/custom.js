(function ($) {
  Drupal.behaviors.mainMenu = {
    attach: function (context, settings) {

      $(document).ready(function() {
          $(".navToggle a").click(function(e){
              e.preventDefault();

              $("header nav").slideToggle("medium");
              $(".header_logo").toggleClass("menuUp menuDown");
          });

          $(window).resize(function() {
              if($( window ).width() >= "600") {
                  $("header > nav").css("display", "block");

                  if($(".header_logo").attr('class') == "menuDown") {
                      $(".header_logo").toggleClass("menuUp menuDown");
                  }
              }
              else {
                  $("header > nav").css("display", "none");
              }
          });

          $("header > nav > ul > li > a").click(function(e) {
              if($( window ).width() <= "768") {
                  if($(this).siblings().size() > 0 ) {
                      $(this).siblings().slideToggle("fast")
      				$(this).children(".toggle").html($(this).children(".toggle").html() == 'close' ? 'expand' : 'close');
                  }
              }
          });
      });

    }
  };
}(jQuery));
